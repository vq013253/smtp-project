package networking;

import java.net.InetAddress;
import java.net.Socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class SMTPClient {
	/**
	 * Represents the attributes of the client.
	 * 
	 * @param port
	 * @param Socket       s
	 * @param serverAdress
	 */
	private int port;
	private Socket s;
	private static InetAddress serverAddress;

	public SMTPClient(InetAddress serverAddress, int portNum) { // Constructor for the client
		SMTPClient.serverAddress = serverAddress; // Client ip
		portNum = port; // Client port
	}

	/**
	 * Getters and setters for the client
	 * 
	 * @param return         port
	 * @param return         serverAddress
	 * @param this.port=port
	 */

	public SMTPClient() {
	}

	public int getPort() {
		return port;
	}

	public InetAddress getServer() {
		return serverAddress;
	}

	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Socket creation the send method and the listen method are all in one function
	 */
	public void createSocket(InetAddress serverAddress, int port) throws IOException {
		s = new Socket(serverAddress, port); // socket connection

		DataOutputStream dataOut = new DataOutputStream(s.getOutputStream()); // send method
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in)); // also part of send

		while (true) { // listen method
			String so = bufReader.readLine();
			dataOut.writeUTF(so);// messege to server
			if (so.equalsIgnoreCase("QUIT")) {
				System.out.println("Connection closed by Client!");
				break;
			}
		}
		s.close();
	}

	/**
	 * Main of our client
	 * 
	 * @param serverIP
	 * @param serverport
	 */
	public static void main(String[] args) throws IOException {

		InetAddress serverIp = InetAddress.getLocalHost(); // init local host ip
		int port = 25; // port number
		if (args.length > 0) { // if statement for the jar file
			serverIp = InetAddress.getByName(args[0]);
			port = Integer.parseInt(args[1]);
		}

		SMTPClient c = new SMTPClient(serverIp, port); // init object SMTPclient
		c.createSocket(serverIp, port); // calling the function create socket
	}
}