package networking;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

class SMTPHandler extends Thread {
	// Attributes for the thread so that it can be assigned
	final BufferedReader bufReader;
	final DataInputStream dataOut;
	final ServerSocket s;

	// Constructor for the thread
	public SMTPHandler(ServerSocket s, BufferedReader bufReader, DataInputStream dataOut) {
		this.s = s;
		this.bufReader = bufReader;
		this.dataOut = dataOut;
	}
}

public class SMTPServer extends SMTPClient {
	/**
	 * Attributes for our class SMTPserver/object
	 */
	private ServerSocket s;
	private Socket ss;
	public static SMTPClient c = new SMTPClient();
	private ArrayList<String> initialMsgs = new ArrayList<String>();

	/**
	 * Getters and setters for the client
	 * 
	 * @param return s
	 * @param return ss
	 * @param this.s
	 * @this this.ss
	 */
	public ServerSocket getS() {
		return s;
	}

	public Socket getSs() {
		return ss;
	}

	public void setS(ServerSocket s) {
		this.s = s;
	}

	public void setSs(Socket ss) {
		this.ss = ss;
	}

	/**
	 * initial messeges function using array list for dynamic change if needed
	 */
	public String addToList() {
		String message = "";
		initialMsgs.add("S: 220 gov.uk");
		initialMsgs.add("C: HELLO reading.ac.uk");
		initialMsgs.add("S: 250 Hello reading.ac.uk, pleased to meet you");
		initialMsgs.add("C: MAIL FROM: <student@reading.ac.uk>");
		initialMsgs.add("S: 250 ok");
		initialMsgs.add("C: RCPT TO: <student-loan@gov.uk>");
		initialMsgs.add("S: 250 ok");
		initialMsgs.add("C: DATA");
		initialMsgs.add("S: 354 End data with <CR><LF>.<CR><LF>");
		initialMsgs.add("C: Hello loan team,");
		initialMsgs.add("C: If I don't study, will I lose my loan?");
		initialMsgs.add("C: .");
		initialMsgs.add("S: 250 ok Message accepted for delivery");
		initialMsgs.add("C: QUIT");
		initialMsgs.add("S: 221 gov.uk closing connection");
		for (int i = 0; i < initialMsgs.size(); i++) {
			message += initialMsgs.get(i) + "\n";
		}
		return message;
	}

	/**
	 * a part of the send method that is called in function later
	 */
	public void messages(String str, BufferedWriter newWriter) throws IOException {
		if (str.contains("HELLO")) {
			System.out.println("S: 250 " + str.toLowerCase() + " ,pleased to meet you");
		} else if (str.contains("MAIL FROM:")) {
			System.out.println("S: 250 " + "ok");
			newWriter.write(str);
			newWriter.newLine();
			newWriter.flush();
		} else if (str.contains("RCPT TO:")) {
			System.out.println("S: 250 " + "ok");
			newWriter.write(str);
			newWriter.newLine();
			newWriter.flush();
		} else if (str.equalsIgnoreCase("DATA")) {
			System.out.println("S: 354 End data with <CR><LF>.<CR><LF>");
		} else if (!str.contains(".")) {
			newWriter.write(str);
			newWriter.newLine();
			newWriter.flush();
		} else if (str.contains(".")) {
			System.out.println("S: 250 ok Message accepted for delivery");
		}

	}

	/**
	 * Socket creation the send method and the listen method are all in one function
	 */
	public void createSocket(InetAddress serverAddress, int port, SMTPServer srv) throws IOException {
		s = new ServerSocket(port); // create socket
		ss = s.accept(); // Listens for a connection to be made to this socket and accepts it. The method
							// blocks until a connection is made.

		System.out.println(srv.addToList());// calling the initial messages method

		DataInputStream dataOut = new DataInputStream(ss.getInputStream()); // listen method
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));// also part of the listen
																						// method

		BufferedWriter writer = new BufferedWriter(new FileWriter("text.txt")); // method for saving the needed data on
																				// a txt file

		Thread t = new SMTPHandler(s, bufReader, dataOut);
		t.start();

		while (true) {
			String msg = dataOut.readUTF(); // send method that gives us adequate responses
			System.out.println("C: " + msg);
			srv.messages(msg, writer);
			if (msg.equalsIgnoreCase("QUIT")) {
				System.out.println("S: 221 gov.uk closing connection");
				break;
			}
		}
		ss.close();
	}

	/**
	 * Main of our client
	 * 
	 * @param serverIP
	 * @param serverport
	 */
	public static void main(String[] args) throws IOException {
		InetAddress serverIP = InetAddress.getLocalHost();
		int port = 25;
		if (args.length > 0) {
			serverIP = InetAddress.getByName(args[0]);
			port = Integer.parseInt(args[1]);
		}
		SMTPServer ser = new SMTPServer();
		ser.createSocket(serverIP, port, ser);
	}

}